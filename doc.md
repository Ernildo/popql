# Alteração na PopQl 
 A alteração na PopQl resulta em uma quebra de compatibilidade por conta dos seguites motivos:

### Sobre o método *then()*
Antes o método **then()** recebia uma callback com apenas um parâmetro que já era a resposta formatada do servidor. Agora, por uma questão de coerência, o mesmo método **then()** recebe uma callback com as seguintes caracteristícas:
```js
popQl
  .query({
    // configurações padrão
  })
  .then(({ data, error }) => {
    // instruções da função callback
  })
```
Basicamente se a comunicação com o servidor for consolidada, a função callback do método **then()** é invocada recebendo um objeto que contêm o resultado da consulta(*data*) e os possíveis os erros(*error*).

#### Exemplo prático 1
O servidor devolve um resultado positivo para o cliente
```js
popQl.query({
  gql: {
    method: 'getRecurso', 
    // configurações padrão
  }
}).then(({ data, error }) => {
  const { getRecurso } = data

  console.log(getRecurso)  // recurso 
  console.log(error)       // null ou undefined
})
```

#### Exemplo prático 2
O servidor não devolve um resultado positivo para o cliente

```js
popQl.query({
  gql: {
    method: 'getRecurso', 
    // configurações padrão
  }
}).then(({ data, error }) => {
  console.log(data?.getRecurso)   // null ou undefined 
  console.log(error)              // erros 
})
```

### Sobre o método *catch()*
Na nova abordagem a função callback do método **catch()** só será invocada em duas situações:

1. **Porblemas na comunicação cliente/servidor**<br>
Conexão ruim ou perda de pacotes podem gerar erros relacionados a comunicação entre o cliente e o servidor
2. **Passagem errada de parâmetros**<br>
A PopQl identifica a passagem errada de prâmetros em seus métodos