const PopQl = require('../lib/index')

//const PopQl = require('popql')

const popQl = new PopQl({
  endPoint: "http://162.241.130.77:80/",
  headers: {
    Authorization: "bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOjEsImlhdCI6MTU4NDEwMjI3N30.6tYHLQGcCYsYETu3icih1szKVy1k5zdnnACDojfP5Gg",
    db: "sf_teste"
  }
})

popQl.query({
  //show: true,
  gql: {
    method: 'getFuncionarios',
    fields: ['id', 'nome']
  }
}).then(({ data }) => data)
  .then(({ getFuncionarios }) => getFuncionarios.filter(e => e.nome === 'Pessoa 2'))
  .then(res => res.map(el => el.nome).join(' | '))
  .then(console.log)
  .catch(console.log)
/*
popQl.mutation({
  show: true,
  gql: {
    method: 'insertFuncionarioEmFolhaPagamento',
    data: {
      folha_id: 54,
      funcionario_id: 2
    },
    fields: ['id']
  }
}).then(({ data, error }) => console.log(data, error))
  .catch(err => console.log('entrou no catch', err))
*/
/*
popQl.mutation({
show: true,
gql: {
  standard: false,
  method: "createPessoa",
  data: {
    nome: "Teste 110",
    cpf: "3334443",
    dependentes: []
  },
  fields: ['id']
}
}).then(res => console.log(res))
.catch(err => console.log(err))*/