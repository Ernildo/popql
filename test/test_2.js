/*
DESCRIÇÃO DA INCOERÊNCIA DO BACKEND:
O objeto 'res.data'(NA REQUISIÇÃO) contém dois objetos: data e errors.
Por padrão a PopQl estava condicionada a validar o res.data.data, caso fosse null
a o método reject é invocado passado o 'erros' como parâmetro(fazendo com que um catch seja invocado).
Toda via, para o caso de teste deste aquivo, o res.data.data não vinha null e sim o res.data.data['método']
o que acaba gerando um falso positivo na validação da pop e com isso o errors nunca chegava no usuário.
*/

const PopQl = require('../lib/index');

data = {
  id: 22,
  matricula: "34278",
  pessoa_id: 2,
  status: "ativo",
  vinculo_id: 2,
  parametro: null,
  cargo_lotacao: { cargo_id: 2, carga_horaria_semanal: 30 },
  data_admissao: "2020-09-23",
  data_posse: "2020-09-23",
  data_reintegracao: "2020-09-23",
  data_ats: "2020-09-23",
  data_progressao: "2020-09-24",
  fim_contrato: "2020-09-30",
  tipo_admissao: "Primeiro emprego",
  tempo: "2020-09-25",
  matricula_anterior: "341",
  numero_contrato: "2315",
};

const popQl = new PopQl({
  endPoint: "http://162.241.130.77:80/",
  headers: {
    Authorization: "bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOjEsImlhdCI6MTU4NDEwMjI3N30.6tYHLQGcCYsYETu3icih1szKVy1k5zdnnACDojfP5Gg",
    db: "sf_teste"
  }
})

popQl
  .mutation({
    show: true,
    gql: {
      data,
      method: "updateFuncionario",
      fields: ["id"],
    },
  })
  .then(({ data, error }) => {
    console.log('data: ', data)
    console.log('error: ', error)
  })
  .catch((err) => {
    console.error(err);
  });