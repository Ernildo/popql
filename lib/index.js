const { queryGenerate, mutationGenerate } = require('./func_aux/Generates')
const { filterData, filterParams } = require('./func_aux/filters')
const request = require('./func_aux/request')

const QUERY = true
const MUTATION = false

const exec = (type, params, p) => {
  return new Promise((resover, reject) => {
    try {
      const paramsFiltered = filterData(params)
      const axiosParams = filterParams(params, p)

      const modelQuery = type ? queryGenerate(paramsFiltered) : mutationGenerate(paramsFiltered)

      if (params.show) console.log(modelQuery)

      request(axiosParams, modelQuery)
        .then(resover)
        .catch(reject)

    } catch (err) {
      reject(err)
    }
  })

}

module.exports = function PopQl(p) {
  return {
    query(params) {
      return exec(QUERY, params, p)
    },
    mutation(params) {
      return exec(MUTATION, params, p)
    }
  }
}