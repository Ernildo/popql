const axios = require('axios')

const request = (axiosParams, query) => {
  const data = { query }

  if (axiosParams === undefined || data === undefined)
    throw "a função request espera receber axiosParams e data"

  return axios({ ...axiosParams, data })
    .then(res => {
      const { data, errors } = res.data
      return {
        data: data || null,
        error: errors || null
      }
    })
}

module.exports = request