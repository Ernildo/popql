function filterParams(params, p) {

  if(params == undefined || params == null){
    throw "experava parâmetros na função 'query'"  /// passar essas mesagens para ingles 
  }else {
    if(params.headers === undefined && params.endPoint === undefined) {
      if(p === undefined || p.headers === undefined || p.endPoint === undefined) 
        throw "experava os parâmetros no construtor"
      else {
        return {
          url: p.endPoint,
          method: 'POST',
          headers: p.headers
        }
      }
    }else return {
      url: params.endPoint,
      method: 'POST',
      headers: params.headers
    }
  }
}

function filterData(params) {
  if(params.gql == undefined || params.gql == null)
    throw "parametro 'gql' não foi encontrado"
  else if(!Object.keys(params.gql).length) {
    throw "O conteúdo de 'gql' é vazio"
  }else return params.gql
}

function filterMethodsGql(params) {
  if(Array.isArray(params.gql)) 
    return params.gql.map(el => el.method)
  else return params.gql.method
}

module.exports = {
  filterData,
  filterParams,
  filterMethodsGql
}