const TYPE_ARGS   = 0
const TYPE_OBJECT = 1
const TYPE_ARRAY  = 2 

const NULL    = 0
const STRING  = 1
const ARRAY   = 2
const OBJECT  = 3
const NUMBER  = 4

function whatValue(value) {
  if(value === undefined || value === null) return NULL
  else if(typeof value === 'string') return STRING
  else if(Array.isArray(value)) return ARRAY
  else if(typeof value === 'object') return OBJECT
  else return NUMBER
}

// ISSO É UMA ZONA DE TESTE =================
function getDataArray(value) {
  return `[${value.map(el => { return `${typeValue(el)}` }).join(',')}]`
}
//===========================================

function typeValue(value, standard) { /// a alteração é aqui
  let formatValue;
  switch(whatValue(value)) {
    case NULL:
      formatValue = null
    break
    case STRING: 
      formatValue = `"${value}"`
    break
    case ARRAY:
      if(!standard && !value.length)
        formatValue = '[]'
      else 
        formatValue = value.length ? getData(value, TYPE_ARRAY, standard) : null
    break
    case OBJECT: 
      formatValue = Object.keys(value).length ? getData(value, TYPE_OBJECT, standard) : null
    break
    case NUMBER: 
      formatValue = value
    break
    default: throw "tipo de valor não suportado"
  }
  return formatValue
}

function getArgs(args) {
  if(args == undefined || !Object.keys(args)) return ""

  return `(${Object.keys(args).map((el) => {return `${el}:${typeValue(args[el])}`}).join(",")})`
}

function getFields(fields) {
  if(fields === null || fields === undefined || !fields.length) return ""
  
  return `${fields.join(",")}`
}
 
function getData(data, type, standard) {
  let dataRes;
  switch(type) {
    case TYPE_ARGS: 
      dataRes = `(${Object.keys(data).map(el => `${el}:${typeValue(data[el], standard)}`).join(",")})`
    break
    case TYPE_OBJECT: 
      dataRes = `{${Object.keys(data).map(el => `${el}:${typeValue(data[el], standard)}`).join(",")}}`
    break
    case TYPE_ARRAY:
      dataRes = `[${data.map(el => { return `${typeValue(el, standard)}` }).join(',')}]`  
    break
    default: throw "tipo de dado não é permitido"
  }
  return dataRes
}

function mountQuery(obj) {
  return `${obj.method}${getArgs(obj.args)}{${getFields(obj.fields)}}`
}

function mountMutation(obj) {
  if(obj.standard === undefined) obj.standard = true
  return `${obj.method}${getData(obj.data,TYPE_ARGS, obj.standard)}{${getFields(obj.fields)}}`
}

function queryGenerate(gql) {
  if(Array.isArray(gql)) {
    return `query {${gql.map(el => mountQuery(el)).join(",")}}`
  }
  else return `query {${mountQuery(gql)}}`
}

function mutationGenerate(gql) {
  if(Array.isArray(gql)) {
    return `mutation {${gql.map(el => mountMutation(el)).join(",")}}`
  }
  else return `mutation {${mountMutation(gql)}}` 
}

module.exports = {
  queryGenerate,
  mutationGenerate
}