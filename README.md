# Documentação PopQl

**PopQl** é uma biblioteca JavaScript desenvolvida para facilitar o uso do *GraphQl*.

## Sobre a versão

A versão 1.0.0 possui apenas funcionalidades básicas como execução de _queries_ e _mutations_, sendo assim,
caracteristicas mais avançadas como websocket não foram implementadas neste momento.

## Como Usar

Toda a estrutura do __PopQl__ consiste no uso de objetos e promisses

### Instalação 

Até o momento PopQl tem apenas o gitlab como repositório, sendo assim, para fazer a instalação no seu projeto
digite o seguinte comando: `npm install --save https://gitlab.com/Ernildo/popql.git`

### Importação

Para fazer uso de __PopQl__ no seu código, digite o seguinte comando:
`const PopQL = require("popql")` ou
`import PopQL from "popql"` em ES6 ou superior.

### Iniciando

Logo após instalar a biblioteca em seu projeto e importa-la em seu código, temos um exemplo de como se trabalhar com __PopQl__ da primeira forma.
```
const popQl = new PopQl({
  endPoint: "http://200.0.0.5:80",
  headers: {
    Authorization: "bearer jhfdgjhfgjhfjhgjhfgjhfgjhfg",
    db: "45bg"
  }
})
```
Criamos uma instância da __PopQl__ passado como parâmetro para o contrutor um objeto o de configuração cujo suas propriedades são descritas na tabela abaixo:

Atributo | Obrigatório | Tipo     | Descrição
---------|-------------|----------|------------
*endPoint* | SIM         | String   |  URL do servidor
*headers* | NÃO | Object | objeto que recebe os ítens (atributo : valor) que devem ser enviados no cabeçalho

#### Queries

As queries são as operações que, no geral, não alteram o backend. No __PopQl__ podemos realizar uma _query_ invocando o método __query()__ passando parâmetros como mostrado abaixo.
```
popQl.query({
  gql: {
    /// configurações da query
  }
}).then(res => /* ação */)
  .catch(err => /* ação */)
```
O método __query()__ recebe como parâmetro um objeto cujo sua lista de atributos pode ser estudado na tabela abaixo.

Atributo | Obrigatório | Tipo     | Descrição
---------|-------------|----------|------------
_endPoint_ | NÃO | String | URL do servidor. Caso esse atributo seja passado na query, o endereço contido aqui terá maior prioridade em relação ao atributo da instância.
_headers_ | NÃO | Objeto | Obejto contendo o cabeçalho que deve ser enviado na requisição. Assim como o atributo anterior, caso o _headers_ local seja identificado no objeto passado para o metodo __query()__, o mesmo tem prioridade sobre o _headers_ de instância.
_gql_ | SIM | Objeto | Objeto que informa as diretrizes para a montagem da query.

O método **query()** trabalha com o conceito de promisse, sendo assim, caso a consulta tenha sido realizada com sucesso, o metódo **then()** é invocado. Caso contrário, se alguma excessão foi e detectada, o método **cathc()** é inovcado.

##### sobre o método then()
Este método recebe como parâmetro uma callback que será executada caso a consulta seja realizada com sucesso. A callback passada para o **then()** recebe como parametro o resultado/resposta do servidor. Um exemplo pode ser visto no código abaixo.

```
popQl.query({
  // opoções e diretrizes de consulta
}).then(res => {
  /// a ação pode ser feita a qui dentro
  console.log(res)
})
```
ou 
```
popQl.query({
  // opoções e diretrizes de consulta
}).then(res => myCallback(res))
```
